#!/bin/sh

FILE="prova.conf"

/bin/cat <<EOM >$FILE
ctrl_interface=DIR=/var/wpa_supplicant GROUP=netdev
update_config=1

network={
	ssid="$1"
	psk="$2"
	key_mgmt="WPA-PSK"
}
EOM