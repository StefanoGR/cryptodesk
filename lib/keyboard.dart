import 'package:flutter/material.dart';

class Keyboard extends StatefulWidget {
  const Keyboard({Key? key}) : super(key: key);

  @override
  State<Keyboard> createState() => _KeyboardState();
}

class _KeyboardState extends State<Keyboard> {
  var state = "";
  var keySet = [
    ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
    ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p"],
    ["a", "s", "d", "f", "g", "h", "j", "k", "l"],
    ["z", "x", "c", "v", "b", "n", "m"]
  ];
  var specialKeySet = [
    ["\\", "|", "!", "\"", "£", "\$", "%", "&", "/"],
    ["(", ")", "=", "?", "^", "[", "]", "@", "#"],
    ["§", "<", ">", ";", ",", ":", ".", "-", "_"]
  ];

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      const TextField(
        decoration: InputDecoration(
            border: OutlineInputBorder(), hintText: 'Enter WIFI name'),
      ),
      ...buildKeyBoard(keySet)
    ]);
  }

  List<Widget> buildKeyBoard(List<List<String>> keys) {
    List<Widget> rows = keys.map((element) {
      return Row(
        children: element.map((key) => buildRowKeys(key)).toList(),
      );
    }).toList();

    return rows;
  }

  Widget buildRowKeys(String element) {
    return Container(
      padding: const EdgeInsets.all(1.0),
      child: ElevatedButton(onPressed: () {}, child: Text(element)),
    );
  }
}
