import 'dart:async';
import 'dart:convert';

import 'package:cryptodesk/constants.dart';
import 'package:cryptodesk/domain/repository/entities/symbol.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<List<CryptoSymbol>> fetchCryptos(http.Client client) async {
  final response = await client.get(Uri.parse(BINANCE_CRYPTO_LIST));
  return compute(parseCryptos, response.body);
}

List<CryptoSymbol> parseCryptos(String responseBody) {
  final parsed = jsonDecode(responseBody);

  List<CryptoSymbol> items = parsed['symbols']
      .map<CryptoSymbol>((json) => CryptoSymbol.fromJson(json))
      .toList();
  items.sort((a, b) {
    return a.baseAsset!.compareTo(b.baseAsset!);
  });
  return items;
}

const String CRYPTO_ROUTE = '/cryptolist';

class CryptoList extends StatelessWidget {
  const CryptoList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<CryptoSymbol>>(
        future: fetchCryptos(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return CryptoSymbolsList(symbols: snapshot.data!);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class CryptoSymbolsList extends StatelessWidget {
  const CryptoSymbolsList({Key? key, required this.symbols}) : super(key: key);

  final List<CryptoSymbol> symbols;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconButton(
            icon: const Icon(Icons.arrow_back_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
          ),
          itemCount: symbols.length,
          itemBuilder: (context, index) {
            var symbol = symbols[index];
            return Column(children: [
              ExtendedImage.network(
                "$CRYPTO_ICON_URL${symbols[index].baseAsset}/128x128",
                clearMemoryCacheIfFailed: true,
                fit: BoxFit.fill,
                cache: true,
                loadStateChanged: (ExtendedImageState state) {
                  switch (state.extendedImageLoadState) {
                    case LoadState.completed:
                      return ExtendedRawImage(
                        image: state.extendedImageInfo?.image,
                      );
                    default:
                      return Image.asset(
                        "assets/placeholder.png",
                        fit: BoxFit.fill,
                      );
                  }
                },
              ),
              Text(symbol.symbolFull!),
            ]);
          },
        ),
      ],
    );
  }
}
