import 'dart:convert';
import 'dart:io';

import 'package:cryptodesk/crypto_list.dart';
import 'package:cryptodesk/crypto_list.dart';
import 'package:cryptodesk/domain/repository/entities/ticker.dart';
import 'package:cryptodesk/domain/repository/websockets/websockets_io.dart';
import 'package:flutter/material.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:intl/intl.dart';
import 'package:process_run/shell.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

class CryptoDeskSettings {
  CryptoDeskSettings(StreamingSharedPreferences preferences)
      : symbol = preferences.getString('symbol', defaultValue: "btceur");
  final Preference<String> symbol;
}

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  final preferences = await StreamingSharedPreferences.instance;
  final settings = CryptoDeskSettings(preferences);


  runApp( MyApp(settings:settings));
}

class MyApp extends StatelessWidget {
  final CryptoDeskSettings settings;
  const MyApp( {Key? key, required this.settings}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ConnectivityAppWrapper(
        app: MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const MyHomePage(title: ''),
        CRYPTO_ROUTE: (context) => const CryptoList(),
      },
    ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Ticker? ticker;
  WebsocketBinanceListener websocketBinanceListener =
      WebsocketBinanceListener();
  var priceFormat = NumberFormat("###.0#", "en_US");

  @override
  void initState() {
    websocketBinanceListener.subscribeTicker(
      "btceur",
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder<dynamic>(
      stream: websocketBinanceListener.stream,
      builder: (
        BuildContext context,
        AsyncSnapshot<dynamic> snapshot,
      ) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        } else if (snapshot.connectionState == ConnectionState.active ||
            snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return const Text('Error');
          } else if (snapshot.hasData) {
            var ticker = Ticker.fromJson(jsonDecode(snapshot.data));
            return Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(ticker.symbol,
                            style: const TextStyle(
                                fontSize: 110, fontWeight: FontWeight.w300)),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0, left: 32.0),
                          child: Text(priceFormat.format(ticker.lastPrice),
                              style: const TextStyle(
                                  fontSize: 120, fontWeight: FontWeight.w400)),
                        ),
                        Row(
                          children: [
                            const SizedBox(width: 248.0),
                            const Padding(
                              padding: EdgeInsets.only(right: 16.0),
                              child: Text("24h",
                                  style: TextStyle(
                                      fontSize: 60,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w200)),
                            ),
                            Text("${ticker.priceChangePercent}%",
                                style: const TextStyle(
                                    fontSize: 60, fontWeight: FontWeight.w200)),
                          ],
                        ),
                        Expanded(child: Container()),
                        StreamBuilder(
                            stream:
                                Stream.periodic(const Duration(seconds: 10)),
                            builder: (context, snapshot) {
                              var now = DateTime.now();
                              return Padding(
                                padding: const EdgeInsets.only(right: 16.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(DateFormat('HH:mm').format(now),
                                        style: const TextStyle(
                                            fontSize: 50,
                                            color: Colors.black54,
                                            fontWeight: FontWeight.w300)),
                                    Text(
                                        DateFormat('E d MMM yyyy')
                                            .format(now)
                                            .toUpperCase(),
                                        style: const TextStyle(
                                            fontSize: 38,
                                            color: Colors.black45,
                                            fontWeight: FontWeight.w300)),
                                  ],
                                ),
                              );
                            }),
                        // ),
                        // // Container(height: 200,
                        // //   child: ElevatedButton(
                        // //     onPressed: _checkWifi,
                        // //     child: const Text("PROVA PROVA PROVA PROVA PROVA PROVA PROVA PROVA PROVA PROVA PROVA "),
                        // //   ),
                        // // ),
                        // // Text(
                        // //   state,
                        // //   style: Theme.of(context).textTheme.headline1,
                      ],
                    ),
                  ),
                ),
                Container(
                  width: 200.0,
                  color: ticker.priceChangePercent > 0
                      ? Colors.green[300]
                      : Colors.red[300],
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      FloatingActionButton(
                        onPressed: () {
                          Navigator.pushNamed(context, CRYPTO_ROUTE);
                        },
                      )
                    ],
                  ),
                )
              ],
            );
          } else {
            return const Text('Empty data');
          }
        } else {
          return Text('State: ${snapshot.connectionState}');
        }
      },
    ));
  }

  void _checkWifi() async {
    // if (await ConnectivityWrapper.instance.isConnected) {
    //
    //   setState(() {
    //     state = "conn";
    //   });
    //   // I am connected to a wifi network.
    // }else {
    //   setState(() {
    //     state = "NOT";
    //   });
    // }
    var shell = Shell();

    await shell.run('./script.sh ciao ciao');
  }
}
