class CryptoSymbol {
  final String? symbolFull;
  final String? status;
  final String? baseAsset;
  final int? baseAssetPrecision;
  final String? quoteAsset;
  final int? quotePrecision;
  final int? quoteAssetPrecision;
  final int? baseCommissionPrecision;
  final int? quoteCommissionPrecision;

  const CryptoSymbol(
      {this.symbolFull,
        this.status,
        this.baseAsset,
        this.baseAssetPrecision,
        this.quoteAsset,
        this.quotePrecision,
        this.quoteAssetPrecision,
        this.baseCommissionPrecision,
        this.quoteCommissionPrecision,
      });

  factory CryptoSymbol.fromJson(Map<String, dynamic> json) {
    return CryptoSymbol(
    symbolFull : json['symbol'],
    status  : json['status'],
    baseAsset  : json['baseAsset'],
    baseAssetPrecision  : json['baseAssetPrecision'],
    quoteAsset  : json['quoteAsset'],
    quotePrecision  : json['quotePrecision'],
    quoteAssetPrecision  : json['quoteAssetPrecision'],
    baseCommissionPrecision  : json['baseCommissionPrecision'],
    quoteCommissionPrecision  : json['quoteCommissionPrecision']);
  }

}