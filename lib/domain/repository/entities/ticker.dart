// To parse this JSON data, do
//
//     final ticker = tickerFromJson(jsonString);

import 'dart:convert';

Ticker tickerFromJson(String str) => Ticker.fromJson(json.decode(str));

String tickerToJson(Ticker data) => json.encode(data.toJson());

class Ticker {
  Ticker({
    required this.tickerE,
    required this.timeStamp,
    required this.symbol,
    required this.priceChangePercent,
    required this.p,
    required this.w,
    required this.x,
    required this.lastPrice,
    required this.q,
    required this.tickerB,
    required this.b,
    required this.tickerA,
    required this.a,
    required this.tickerO,
    required this.h,
    required this.tickerL,
    required this.v,
    required this.tickerQ,
    required this.o,
    required this.c,
    required this.f,
    required this.l,
    required this.n,
  });

  String tickerE;
  int timeStamp;
  String symbol;
  double priceChangePercent;
  String p;
  String w;
  String x;
  double lastPrice;
  String q;
  String tickerB;
  String b;
  String tickerA;
  String a;
  String tickerO;
  String h;
  String tickerL;
  String v;
  String tickerQ;
  int o;
  int c;
  int f;
  int l;
  int n;

  factory Ticker.fromJson(Map<String, dynamic> json) => Ticker(
    tickerE: json["e"],
    timeStamp: json["E"],
    symbol: json["s"],
    priceChangePercent: double.parse(json["P"]),
    p: json["P"],
    w: json["w"],
    x: json["x"],
    lastPrice: double.parse(json["c"]),
    q: json["Q"],
    tickerB: json["b"],
    b: json["B"],
    tickerA: json["a"],
    a: json["A"],
    tickerO: json["o"],
    h: json["h"],
    tickerL: json["l"],
    v: json["v"],
    tickerQ: json["q"],
    o: json["O"],
    c: json["C"],
    f: json["F"],
    l: json["L"],
    n: json["n"],
  );

  Map<String, dynamic> toJson() => {
    "e": tickerE,
    "E": timeStamp,
    "s": symbol,
    "p": priceChangePercent,
    "P": p,
    "w": w,
    "x": x,
    "c": lastPrice,
    "Q": q,
    "b": tickerB,
    "B": b,
    "a": tickerA,
    "A": a,
    "o": tickerO,
    "h": h,
    "l": tickerL,
    "v": v,
    "q": tickerQ,
    "O": o,
    "C": c,
    "F": f,
    "L": l,
    "n": n,
  };
}
