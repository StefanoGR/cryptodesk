import 'dart:async';
import 'dart:convert';
import 'package:cryptodesk/constants.dart';
import 'package:cryptodesk/domain/repository/websockets/ws_response_models.dart';
import 'package:web_socket_channel/io.dart';

class WebsocketBinanceListener {
  IOWebSocketChannel? socket;
  Stream<dynamic>? stream;
  int? closeCode;
  Timer? _keepAliveTimer;
  StreamSubscription? mainSupscription;

  void _subscribe<DataModel>(String connectionJsonMessage) {
    closeCode = null;
    socket = IOWebSocketChannel.connect(BINANCE_WEBSOCKET);
    stream = socket?.stream.asBroadcastStream();
    mainSupscription = stream?.listen((message) {

    }, onDone: () {
      if (closeCode == 1000) {
      } else {
        _subscribe(connectionJsonMessage);
      }
    });

    socket?.sink.add(connectionJsonMessage);
    _keepAliveTimer?.cancel();
    _keepAliveTimer = Timer.periodic(const Duration(minutes: 25), (timer) {
      try {
        socket?.sink.add(json.encode({'method': 'keepAlive'}));
      } catch (e) {
        _subscribe(connectionJsonMessage);
      }
    });
  }



  void subscribeTicker(String marketPairSymbol) {
    _subscribe<MarketDepthData>(
        json.encode({
          'method': 'SUBSCRIBE',
          "params": [
            "$marketPairSymbol@ticker"
          ],
          "id": 1
        }));
  }

  Future<void> close() async {
    socket?.sink.add(json.encode({'method': 'close'}));
    closeCode = 1000;
    await socket?.sink.close(1000);
    await mainSupscription?.cancel();
    _keepAliveTimer?.cancel();
  }

  void unsubscribeTopic(String topic) {
    socket?.sink.add(json.encode({'method': 'unsubscribe', 'topic': topic}));
  }

  void unsubscribeOrderBookSymbol(String symbol) {
    socket?.sink.add(json.encode({'method': 'unsubscribe', 'topic': 'marketDepth', symbol: symbol}));
  }
}

class WebsocketBinanceManager {
  Map<String, WebsocketBinanceListener>? sockets;

  WebsocketBinanceManager();
}
